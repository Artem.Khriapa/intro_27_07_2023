# file = open('example.txt')
# for line in file:
#     print(line)
# file.close()

# file = open('example.txt', 'w')
# file.write('hello World!')
# file.close()


# with open('example.txt') as file:  # file = open('example.txt')
#     for line in file:
#         print(line)
#     # file.close()
