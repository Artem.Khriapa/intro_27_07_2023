# namespaces


# global_var = 100


# def foo(global_var):
#
#     print('in function - global_var', global_var)
#     global_var += 1
#     print('in function - global_var', global_var)
#     return global_var
#
#
# print('outside function - global_var', global_var)
# global_var = foo(global_var)
# print('outside function - global_var', global_var)

# global_var = [1, 2, 3]
#
#
# def foo(global_var):
#     print('in function - global_var', global_var)
#     global_var.append('a')
#     print('in function - global_var', global_var)
#     return global_var
#
#
# print('outside function - global_var', global_var)
# global_var = foo(global_var)
# print('outside function - global_var', global_var)


# global_var = 100


# def bar():
#     print('in bar')
#
#
# def foo():
#     global global_var
#     bar()
#     print('in function - global_var', global_var)
#     global_var = [1, 2, 3, 4]
#     print('in function - global_var', global_var)
#
#
# print('outside function - global_var', global_var)
# foo()
# print('outside function - global_var', global_var)

# global_var = 1


# def foo():
#     global global_var
#     print('in function - global_var', global_var)
#     global_var = [1, 2, 3, 4]
#     print('in function - global_var', global_var)
#
#
# print(dir())
# foo()
# print(dir())


# recursion

# n! = n * f(n-1)


# def factorial_rec(n):
#     if n <= 1:
#         return 1
#     return n * factorial_rec(n-1)
#
#
# print(factorial_rec(5))


# 1 1 2 3 5 8 13 21 ...


# n = f(n-1) + f(n-2)


# counter = 0
#
#
# def fib_rec(n):
#     global counter
#     counter += 1
#
#     if n in (1, 2):
#         return 1
#
#     return fib_rec(n-1) + fib_rec(n-2)


# print(fib_rec(40))
# print('counter', counter)


# counter = 0
#
# # 1 1 2 3 5 8 13 21 ...
#
#
# def fib_plain(n):
#     global counter
#     counter += 1
#
#     fib1 = fib2 = 1
#     if n in (1, 2):
#         return fib1
#
#     for i in range(n-2):
#         fib1, fib2 = fib2, fib1 + fib2
#
#     return fib2
#
#
# print(fib_plain(1000))
# print('counter', counter)


# decorator


# def foo(*args, **kwargs):
#     print('in foo')
#     print(args)
#     print(kwargs)
#
#
# def my_function(func, *args, **kwargs):
#     print('in my_function')
#     res = func(*args, **kwargs)
#     return res
#
#
# res = my_function(foo, 1, 2, 3, a=10, b=20)
# print(res)
#
# res = my_function(int, '123')
# print(res)

#
# def function_creator(arg1):
#
#     def foo(*args, **kwargs):
#         print(f'in foo {arg1}')
#         print(args)
#         print(kwargs)
#
#     return foo
#
#
# res = function_creator('abcd')
#
# print(res)
# res(1, 2, 3, a=10)


# def function_decorator(func):
#
#     def _wrapper(*args, **kwargs):
#         print('before function')
#         res = func(*args, **kwargs)
#         print('after function')
#
#         return res
#
#     return _wrapper
#
#
# def foo(*args, **kwargs):
#     print(f'in foo')
#
#
# # decorated_function = function_decorator(foo)
# #
# # # print(type(decorated_function))
# #
# #
# # decorated_function(1, 2, 3, a=10)
#
#
# foo = function_decorator(foo)
#
# foo(1, 2, 3)


# def function_decorator1(func):
#
#     def _wrapper(*args, **kwargs):
#         print('before function 1')
#         res = func(*args, **kwargs)
#         print('after function 1')
#
#         return res
#
#     return _wrapper
#
#
# def function_decorator2(func):
#
#     def _wrapper(*args, **kwargs):
#         print('before function 2')
#         res = func(*args, **kwargs)
#         print('after function 2')
#
#         return res
#
#     return _wrapper
#
#
# @function_decorator2
# @function_decorator1  # foo = function_decorator2(function_decorator1(foo))
# def foo(*args, **kwargs):
#     print(f'in foo')
#
#
# foo(1, 3, 4)

#
# def only_int_decorator(func):
#
#     def _wrapper(*args, **kwargs):
#         print('only_int_decorator')
#
#         for arg in args:
#             if not isinstance(arg, int):
#                 raise TypeError
#         for key, val in kwargs.items():
#             if not isinstance(val, int):
#                 raise TypeError
#
#         res = func(*args, **kwargs)
#
#         return res
#
#     return _wrapper
#
#
# @only_int_decorator  # foo = only_int_decorator(foo)
# def foo(arg1, arg2):
#     print('foo---> ', type(arg1), type(arg2))
#
#
# foo(2, 2)


def type_checker(*types):
    def _decorator(func):
        def _wrapper(*args, **kwargs):
            print('type_checker')
            for arg in args:
                if not isinstance(arg, types):  # isinstance(arg, (int, float)))
                    raise Exception
            for key, val in kwargs.items():
                if not isinstance(val, types):
                    raise Exception
            res = func(*args, **kwargs)
            return res
        return _wrapper
    return _decorator


@type_checker(int, float)  # decorator = type_checker(int, float); foo = decorator(foo)
# foo = type_checker(int, float)(foo)
def foo(arg1, arg2):
    print('foo---> ', type(arg1), type(arg2))


@type_checker(str)  # foo = only_int_decorator(foo)
def bar(arg1, arg2):
    print('bar---> ', type(arg1), type(arg2))


foo(1.2, 3)

bar('1.2', '3')


import time
print(time.time())
print(time.time())



# annotation/typing
# docstring
