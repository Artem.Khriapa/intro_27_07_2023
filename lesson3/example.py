# my_str = '1234567'
# print(my_str[2:4])
#
# print(len(my_str))
# print('5' in my_str)
#
# my_str = 'abcabcabcab'
#
# res = my_str.split('c')
# print(res)
# print(type(res))

# # list
#
# my_str = '1234567'
#
# my_list = [1, 2.2, my_str, True, None, 1]
#
# print(my_list)
#
# my_list.append(100)
#
# print(my_list)
#
# my_list.remove(2.2)
#
# print(my_list)
#
# print(my_list[1:3])
#
# my_list[1] = False
#
# print(my_list)
#
#
# my_list += ['a', 'b', 'c']
#
# print(my_list)
#
# my_list *= 2
#
# print(my_list)
#
# my_list.extend(['a', 'b', 'c'])
#
# print(my_list)
#
# my_list.pop(0)
#
# print(my_list)
#
# my_list = []  # list()
#
#
# my_list = [1, 2.2, my_str, True, None, 1]
#
# my_str = str(my_list)
# print(my_str)
#
# print(bool(my_list))
# print(bool([]))
#
# print(len(my_list))
# print(2.3 in my_list)

# idx = 0
# my_list = [1, 2.2, 44, 3, 'my_str', True, 55, None, 1000]
# last_idx = len(my_list)
#
# while idx < last_idx:
#
#     value = my_list[idx]
#     if type(value) == int:
#         print(value)
#     idx += 1


# idx = 0
# my_str = 'q1wer3tyul3;lkjh4gfxz2xcv1bnm'
# last_idx = len(my_str)
#
# while idx < last_idx:
#
#     value = my_str[idx]
#     if value.isdigit():
#         print(value)
#     idx += 1


# my_list = [1, 2.2, 44, 3, 'my_str', True, 55, None, 1000]
#
# for list_element in my_list:
#     if type(list_element) == int:
#         print(list_element)

#
# for str_element in 'ad2sf4gdh4fjk6glh;j':
#     if str_element.isdigit():
#         print(str_element)


# for number in range(3):
#     print(number)

# my_list = [1, 2.2, 44, 3, 'my_str', True, 55, None, 1000]
#
#
# for list_element in my_list[:5]:
#     print(type(list_element))
#
# lst = []
#
# for _ in range(3):
#     lst.append(input('Enter smth: '))
#
# print(lst)
#
# lst.pop(1)
#
# lst = [1, 2, 3, ['a', '3', '1']]
#
# print(type(lst[-1]))
#
# lst[-1].append([1, 2, 3])
#
# print(lst)
#
# print(lst[-1][-1])
# print(lst[-1][-1][2])

#
# my_list = list('1234567')
# print(my_list)
#
# my_list = []
# for i in '1234567':
#     my_list.append(i)
#
# my_list = list(range(10))
# print(my_list)
#
# print(id(my_list))
# my_list.append(1)
# print(id(my_list))
#
# my_str = '123456'
# print(id(my_str))
# my_str += '2345'
#
# print(id(my_str))


# tuple

# my_tuple = (1, 2.2, [1, 3], "12345")
#
# print(my_tuple)
# print(type(my_tuple))
#
# print(my_tuple[1])
# print(my_tuple[1:4])
#
# print(my_tuple * 2)
#
# my_tuple += ('a', 'b')
# print(my_tuple)

# my_tuple = (1, 2.2, [1, 3], "12345")
#
# for i in my_tuple:
#     print(i)
#
# lst = list(my_tuple)
# print(lst)
#
# my_tuple = tuple(lst)
# print(my_tuple)
#
# my_tuple = tuple()
# print(my_tuple)


# my_str = 'abcd'
#
# print(my_str == 'abcd')
# print([1, 2, 3] == [1, 3, 3])
# print((1, 2, 3) == (1, 2, 3))

# print('a24' > '124')
#
# print(ord('a'))
# print(ord('A'))

# print('a' == 'а')
#
# print(ord('а'))


# my_tuple = (1, 2.2, [1, 3], "12345")
#
# for tuple_item in my_tuple:
#     print(tuple_item)

# my_list = [1, 2.2, [1, 3], (1, 2, 3), 2, "12345"]
#
# for list_item in my_list:
#     # print(type(list_item), list_item)
#     if type(list_item) in (list, tuple, str):
#         for sub_item in list_item:
#             print(f'subitem ---> {sub_item}')
#     else:
#         print(f'item ---> {list_item}')


# lst = [1, 'asdf', True]
#
# value = None

#
# try:
#     value = lst[0] / int(input('Entet the divisor: '))
# # except ZeroDivisionError:
# #     print('It\'s zero')
# # except ValueError:
# #     print('It\'s not a number')
# # except (ZeroDivisionError, ValueError):
# #     print('something wrong')
# # except Exception:
# #     print('something wrong')
# except Exception as exc:
#     print(f'something wrong {exc}')
# else:
#     print(value)
# finally:
#     print('The end')


while True:
    try:
        value = int(input('Enter the number: '))
    except ValueError:
        print('It\'s not a number!')
    # except Exception:
    #     print('Wrong')
    else:
        print('It\'s ok')
        break

print(f'Number is {value}')



