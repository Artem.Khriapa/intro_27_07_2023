# Descriptor
#   as class

# Magic methods
#   getattribute
#   getattr
#   setattr
#   getitem
#   setitem

# Abstract class


# class MyClass:
#
#     my_attr = 0
#     my_attr_2 = 10
#
#     def __init__(self, val):
#         self.my_attr = val
#
#     def foo(self):
#         pass

    # def __getattribute__(self, item):
    #     print('in __getattribute__', item)
    #     result = super().__getattribute__(item)
    #     print('in __getattribute__ res', result)
    #     return result

    # def __getattr__(self, item):
    #     print('in __getattr__', item)
    #     return None

    # def __setattr__(self, key, value):
    #     print('in __setattr__', key, value)
    #     self.__dict__[key] = value


# obj = MyClass(10)

# print(obj.my_attr)
# getattr(obj, 'my_attr')
# print(obj.__dict__)
# print(obj.my_attr_2)
# print(obj.__class__.__dict__)
# print(obj.myattr2)

# obj.myattr2 = 20
# print(obj.myattr2)

#
# class MyClass:
#
#     my_attr = 10
#
#
# obj1 = MyClass()
# obj2 = MyClass()
#
# print(obj1.my_attr)
# print(obj2.my_attr)
#
# obj1.my_attr = 20
#
# # print(obj1.__dict__)
# # print(obj2.__dict__)
#
# MyClass.my_attr = 100
#
# print(obj1.my_attr)
# print(obj2.my_attr)


# class MyClass:
#
#     my_attr = 10
#
#     def __getitem__(self, item):
#         print('In __getitem__', item)
#         return self.__dict__.get(item, self.__class__.__dict__.get(item))
#
#     def __setitem__(self, key, value):
#         print('In __setitem__', key, value)
#         self.__dict__[key] = value
#
#
# obj1 = MyClass()
#
# print(obj1['my_attr'])
# print(obj1[1])
# obj1[1] = 100
# print(obj1[1])

# class SingletOne:
#
#     _instance = None
#
#     def __new__(cls, *args, **kwargs):
#
#         if cls._instance is None:
#             cls._instance = super().__new__(cls)
#         print('__new__')
#
#         return cls._instance
#
#
# s1 = SingletOne()
# s2 = SingletOne()
#
# print(id(s1))
# print(id(s2))

#
# class Animal:
#     def make_sound(self):
#         return self._make_sound()
#
#     def _make_sound(self):
#         raise NotImplementedError
#
#
# class Dog(Animal):
#     def _make_sound(self):
#         print('Woof')
#
#
# class Crocodile(Animal):
#     def _make_sound(self):
#         print('Hsssss')
#
#
# class Cat(Animal):
#     def _make_sound(self):
#         print('Meou')
#
#
# dog = Dog()
# croc = Crocodile()
# cat = Cat()
#
# dog.make_sound()
# croc.make_sound()
# cat.make_sound()


import abc


class Animal(abc.ABC):

    @abc.abstractmethod
    def make_sound(self):
        pass

    def bar(self):
        pass


class Dog(Animal):
    def make_sound(self):
        print('Woof')


class Crocodile(Animal):
    def foo(self):
        pass

    def make_sound(self):
        print('Hsssss')


class Cat(Animal):
    def make_sound(self):
        print('Meou')


dog = Dog()
croc = Crocodile()
cat = Cat()

dog.make_sound()
croc.make_sound()
cat.make_sound()



