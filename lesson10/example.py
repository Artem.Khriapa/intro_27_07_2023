"""
Напишіть функцію, яка отримує 3 аргументи:  перші 2 - числа, третій -
операція (+, -, *, /), яка повинна бути проведена над ними.
У випадку невідомої операції, функція повертає рядок Unknown
operation. Результатом має бути дійсне число з двома знаками
після десяткової крапки.
"""


def dummy_calc(first: int, operation: str, second: int):
    if operation == '+':
        return first + second
    elif operation == '-':
        return first - second
    elif operation == '*':
        return first * second
    elif operation == '**':
        return first ** second
    elif operation == '/' and second != 0:
        return first / second
    else:
        return 'Unknown operation'


result = dummy_calc(1, '/', 4)
print(result)


def dummy_calc2(first: int, operation: str, second: int):
    res = {
        '+': first + second,
        '-': first - second,
        '/': first / second if second else 'Unknown operation',
        '*': first * second,
        '**': first ** second,
    }

    return res.get(operation, 'Unknown operation')


result = dummy_calc2(1, '**', 4)
print(result)


def dummy_calc3(first: int, operation: str, second: int):
    res = {
        '+': lambda a, b: a + b,
        '-': lambda a, b: a - b,
        '/': lambda a, b: a / b if b else 'Unknown operation',
        '*': lambda a, b: a * b,
        '**': lambda a, b: a ** b,
        '//': lambda a, b: a // b if b else 'Unknown operation',
    }

    if func := res.get(operation):
        return func(first, second)
    else:
        return 'Unknown operation'


result = dummy_calc3(1, '//', 4)
print(result)


"""
Напишіть функцію, яка повертає назву пори року для введеного значення 
номера місяця. У випадку некоректного номеру функція повертає 
замість пори року `unknown` 
(12 -> winter, 5 -> summer , 15 -> 'unknown' ... )
"""


def season(number):
    seasons = {
        1: "winter",
        2: "winter",
        3: "spring",
        4: "spring",
        5: "spring",
        6: "summer",
        7: "summer",
        8: "summer",
        9: "autumn",
        10: "autumn",
        11: "autumn",
        12: "winter",

    }

    return seasons.get(number, 'unknown')


def season2(number):
    seasons = {
        (12, 1, 2): "winter",
        (3, 4, 5): "spring",
        (6, 7, 8): "summer",
        (9, 10, 11): "autumn",
    }

    for key in seasons:
        if number in key:
            return seasons[key]

    return 'unknown'


print(season2(-1))


def season3(number):

    match number:
        case 12 | 1 | 2:
            return "winter"
        case 3 | 4 | 5:
            return "spring"
        case 6 | 7 | 8:
            return "summer"
        case 9 | 10 | 11:
            return "autumn"
        case _:
            return "unknown"


print(season3(12))


"""
Напишіть функцію, яка перевіряє, чи рядок є паліндром чи ні. 
Регістр літер, пропуски і знаки пунктуації не враховуйте. 
Паліндром - це слово, фраза або послідовність, яка читається так само 
як зліва направо, так і справа наліво. 
("No 'x' in Nixon" -> True 
Was it a car, or a cat I saw? -> True
A man, a plan, a canal, Panama! -> True
Palindrom -> False)
"""


def is_palindrom(value: str) -> bool:
    value = value.lower()
    processed_value = value

    for char in value:
        if not char.isalpha():
            processed_value = processed_value.replace(char, '')

    # for char1, char2 in zip(reversed(processed_value), processed_value):
    #     if char1 != char2:
    #         return False
    # return True

    if processed_value == processed_value[::-1]:
        return True
    else :
        return False


print(is_palindrom("A man, a plan, a canal, Panamas!"))



