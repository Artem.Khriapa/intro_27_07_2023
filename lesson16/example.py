# Regular Expressions

# import re

#
# my_str = 'abcd aaa 111 @#$'
# tpl = 'a'
#
# res = re.search(tpl, my_str)
#
# print(res)
# print(res.start())
# print(res.end())
# print(res.group())


# my_str = 'abcd aa1a b311 @#$'
# tpl = 'aa'
#
# res = re.search(tpl, my_str)
# print(res)

# META
# . ^ $ () {} [] / | ? * +

# my_str = '123456f@#$'
# # tpl = '[abcdefghijklmnopqrstuvwxyz]'
# # tpl = '[a-z]'
# tpl = '[a-zA-Z0-9]'
#
# res = re.search(tpl, my_str)
# print(res)

# my_str = '123a456f@#$'
#
# tpl = '[a-z][0-9]'
#
# res = re.search(tpl, my_str)
# print(res)

# my_str = 'a4a6f@#$'
# tpl = '^[a-z][0-9][a-z]'
#
# res = re.search(tpl, my_str)
# print(res)
#
# my_str = 'a4a6fa2sd'
# tpl = '[a-z][0-9][a-z]$'
#
# res = re.search(tpl, my_str)
# print(res)

# my_str = 'aa4a6f!a2sd'
# tpl = '[^a-z0-9]'
#
# res = re.search(tpl, my_str)
# print(res)
#
# my_str = 'ada4a6f!aa2sd'
# tpl = 'a{2}'
#
# res = re.search(tpl, my_str)
# print(res)

# my_str = 'ada4asdsf6f!aa2sd'
# tpl = '[a-z]{4}'
#
# res = re.search(tpl, my_str)
# print(res)

# my_str = 'ada4asdsf6f!aa23sd'
# tpl = '[a-z]{2}[0-9]{2}'
#
# res = re.search(tpl, my_str)
# print(res)


# Artem.Khriapa123@gmail-mail.com.ua

# my_str = 'Artem_.Khriapa-123@gmail-mail.com'
# tpl = r'[a-zA-Z0-9-_.]{1,20}@[a-zA-Z0-9-]{1,20}\.[a-zA-Z0-9]'
#
# res = re.search(tpl, my_str)
# print(res)
#
#
# my_str = 'Artem..Khriapa123@gmail-mail.com'
# tpl = r'[a-zA-Z0-9]{1,10}\.{0,1}[a-zA-Z0-9]{1,10}@[a-zA-Z0-9-]{1,20}\.[a-zA-Z0-9]'
#
# res = re.search(tpl, my_str)
# print(res)


# my_str = 'Artem.Khriapa123@gmail-mail.com'
# tpl = r'[a-zA-Z0-9]{1,10}\.?[a-zA-Z0-9]{1,10}@[a-zA-Z0-9-]{1,20}\.[a-zA-Z0-9]'
#
# res = re.search(tpl, my_str)
# print(res)
#
# my_str = 'Artem.Khriapa123@gmail-mail.com'
# tpl = r'[a-zA-Z0-9_]{1,10}\.?[a-zA-Z]{1,10}[0-9]+@[a-zA-Z0-9-]{1,20}\.[a-zA-Z0-9]'
#
# res = re.search(tpl, my_str)
# print(res)
#
# # a* -> a{0,}
# # a? -> a{0,1}
# # a+ -> a{1,}
#
#
# my_str = 'Artem.Khriapa123@gmail-mail.com'
# tpl = r'\w+\.?\w+@[\w-]{1,20}\.\w+'
#
# # \w -> [a-zA-Z0-9_]
# # \W -> [^a-zA-Z0-9_]
#
# res = re.search(tpl, my_str)
# print(res)

# compiled_search = re.compile(tpl)
#
# res = compiled_search.search(my_str)
# print(res)


# Threading


import threading
import time


# def thread_function(name, delay):
#
#     for i in range(100):
#         print(name, i)
#         time.sleep(delay)
#
#
# th1 = threading.Thread(target=thread_function, args=('thread1', 0.2))
# th2 = threading.Thread(target=thread_function, args=('thread2', 0.5))
#
#
# th1.start()
# th2.start()
#
# print('12345')

counter = 0
threads = []

def foo():
    global counter
    for i in range(1_000_000):
        value = counter
        value += 1
        lst = [j for j in range(100)]
        counter = value


for i in range(5):
    thread = threading.Thread(target=foo)
    threads.append(thread)
    thread.start()


for t in threads:
    t.join()

print(counter)



