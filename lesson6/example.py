# Functions


# def my_add(first, second):
#     print(first)
#     print(second)
#     res = first + second
#
#     return res
#
#
# res = my_add(3, 2)
# print(f'result is {res}')


# def my_add():
#     print('first')
#     print('second')
#
#
# res = my_add()
# print(f'result is {res}')


# def my_add(first, second, third):
#     print(first)
#     print(second)
#     res = first + second + third
#
#     return res
#
#
# res = my_add(3, 2, 2)  # positional
# print(f'result is {res}')

#
# def my_add(first, second, third):
#     print(f"first is {first}")
#     print(f"second is {second}")
#     print(f"third is {third}")
#
#     res = first + second + third
#     return res
#
#
# a, b, c = 1, 2, 3
# # res = my_add(c, b, a)  # positional
# # print(f'result is {res}')
#
# # res = my_add(first=c, second=b, third=a)  # named
# # print(f'result is {res}')
#
#
# res = my_add(third=c, first=b, second=a)  # named
# print(f'result is {res}')
#
#
# def my_add(first, second, third, fourth):
#     print(f"first is {first}")
#     print(f"second is {second}")
#     print(f"third is {third}")
#     print(f"fourth is {fourth}")
#
#     res = first + second + third + fourth
#     return res
#
#
# res = my_add(b, a, third=c, fourth=6)  # positional + named
# print(f'result is {res}')


# def my_add(first, second, third, fourth):
#     print(f"first is {first}")
#     print(f"second is {second}")
#     print(f"third is {third}")
#     print(f"fourth is {fourth}")
#
#     return first + second + third + fourth
#
#
# res = my_add(1, 2, third=3, fourth=4)  # positional + named
# print(f'result is {res}')


# def my_add(first, second, third, fourth):
#     print(f"first is {first}")
#     print(f"second is {second}")
#     print(f"third is {third}")
#     print(f"fourth is {fourth}")
#
#     # return first + second + third + fourth
#
# res = my_add(1, 2, third=3, fourth=4)  # positional + named
# print(f'result is {res}')


# def get_user_int():
#
#     while True:
#         try:
#             return int(input('Enter int: '))
#         except Exception:
#             print('Wrong')
#
#
# result = get_user_int()
#
# print(result)


# def my_calc(first, second, third=0, fourth=1):
#     print(f"first is {first}")
#     print(f"second is {second}")
#     print(f"third is {third}")
#     print(f"fourth is {fourth}")
#
#     return (first + second + third) / fourth
#
#
# # res = my_calc(1, 2, third=3, fourth=4)  # positional + named
# # print(f'--> result is {res}')
#
# res = my_calc(1, 3)  # positional + named
# print(f'--> result is {res}')


# def my_calc(first, second, third=None, fourth=None):
#     print(f"first is {first}")
#     print(f"second is {second}")
#
#     if third is not None:
#         print(f"third is {third}")
#     if fourth is not None:
#         print(f"fourth is {fourth}")
#
#     return first + second
#
#
# # res = my_calc(1, 2, third=3, fourth=4)  # positional + named
# # print(f'--> result is {res}')
#
# res = my_calc(1, 3)  # positional + named
# print(f'--> result is {res}')


# def my_calc(first, second):
#     print(f"first:  {id(first)}")
#     print(f"second: {id(second)}")
#
#     return first + second
#
#
# var1 = 1
# var2 = 4
#
# print(id(var1))
# print(id(var2))
#
# res = my_calc(var1, var2)


# def my_calc(first, second):
#     print(f"first:  {id(first)}")
#     print(f"second: {id(second)}")
#     first += 1
#
#     return first + second
#
#
# var1 = 1
# var2 = 4
#
# print(id(var1))
# print(id(var2))
#
# res = my_calc(var1, var2)
#
# print(var1)
# print(var2)

# lst = [1, 2, 3]
#
# print(id(lst))
#
#
# def my_calc(lst):
#     print(f"lst:  {id(lst)}")
#     lst.append(4)
#
#     return lst
#
#
# print(lst)
#
# lst = my_calc(lst)
#
# print(lst)

#
# lst = [1, 2, 3]
#
#
# def add_elem(elem, lst=[]):
#
#     lst.append(elem)
#
#     return lst
#
#
# print(lst)
#
# lst = add_elem(4, lst)
#
# print(lst)
#
# lst2 = add_elem(4)
# print(lst2)
#
# lst = add_elem(5, lst)
# print(lst)
#
# lst3 = add_elem(1)
# print(lst3)
#
# lst3 = add_elem(5)
# print(lst3)
#
# lst3 = add_elem(6)
# print(lst3)


# def my_calc(*args):
#     print(f"type args is {type(args)}")
#     print(f"args is {args}")
#
#     return args
#
#
# # res = my_calc(1, 2, 3, 4, 5, 5)
# # res = my_calc(1, 2)
#
# lst = [1, 3, 4, 5]
#
#
# res = my_calc(*lst)  # my_calc(1, 3, 4, 5)


# def my_calc(first, second=0, *args):
#     print(f"first:  {first}")
#     print(f"second: {second}")
#     print(f"type args is {type(args)}")
#     print(f"args is {args}")
#
#     return args
#
#
# lst = [1, '3', 4, '5']
#
# # res = my_calc(1, 2, 3, 4, 5, 6, 7, 8, 9, 0)
# # res = my_calc(1)
#
# res = my_calc(*lst)
#
# a = 10
# b = 20
#
#
# def my_calc(first=None, second=0, *args):
#     print(f"first:  {type(first)}")
#     print(f"second: {type(second)}")
#     print(f"args: {type(args)}")
#     for arg in args:
#         print(arg)
#
#     return
#
#
# my_calc(*[1, 2, 3])


# def my_calc(first, second=0, *args):
#     print(f"first:  {first}")
#     print(f"second: {second}")
#     print(f"type args is {type(args)}")
#     print(f"args is {args}")
#
#     return args


# my_calc(1, 2, 3)

# def my_calc(first, second, **kwargs):
#     print(f"type kwargs is {type(kwargs)}")
#     print(f"kwargs is {kwargs}")
#
#     return kwargs
#
#
# my_calc(first=2, second=3, s=4, d=5, abc=[])
#

# def my_calc(a, b, c=0, d=10, *args, **kwargs):
#     print(a, b, c, d)
#     print(f"type args is {type(args)}")
#     print(f"args is {args}")
#     print(f"type kwargs is {type(kwargs)}")
#     print(f"kwargs is {kwargs}")
#
#     return kwargs
#
#
# lst = [1, 2, 3, 4, 5]
# dct = {
#     'first': 2,
#     'second': 22,
#     's': 32,
#     'dd': 12,
#     'abc': 92,
# }
#
# my_calc(*lst, **dct)


CONSTANT = 100


def my_calc(a, b=0):
    print(a, b)
    print(CONSTANT)
    return a


lst = []

lst = my_calc(lst, 2)

print(lst)


def to_int(val):
    return int(val)
