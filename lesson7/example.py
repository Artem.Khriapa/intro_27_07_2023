# counter = 0
#
#
# while counter < 10:
#     counter += 1
#
#     if counter % 2:
#         continue
#
#     print(f'---> {counter}')
#
#     #
#     # if not counter % 2:  # 0
#     #     print(f'---> {counter}')
#
#
# def foo(arg1, arg2):
#     print(arg1, arg2)
#
#
# # foo(1, 2)
#
# foo(arg1=1, arg2=2)
# foo(arg2=2, arg1=1)
#
#
# def foo():
#     print('nothing')
#
#
# tpl = 'My name is %s' % 'Artem'
#
# print(tpl)
#
# tpl = 'My name is %s'
# tpl = tpl % 'Artem'
# print(tpl)
#
# tpl = 'My name is {}'
# res = tpl.format('Artem')
# print(res)
#
# name = 'Artem'
# tpl = 'My name is {}'
# res = tpl.format(name)
# print(res)
#
# name = 'Artem'
# tpl = f'My name is {name}'
# print(res)


# def get_int_from_user():
#
#     while True:
#         number = input('Enter some intreger: ')
#         if number.isdigit():
#             return int(number)
#
#
# def get_int_from_user():
#
#     while True:
#         try:
#             result = int(input('Enter some intreger: '))
#         except (TypeError, ValueError):
#             print('Wrong!')
#         else:
#             return result
#
#
# def get_int_from_user():
#
#     for attempt in range(3):
#         try:
#             result = int(input('Enter some intreger: '))
#         except (TypeError, ValueError):
#             print('Wrong!')
#         else:
#             return result
#
#     print(f'You looser after {attempt + 1} attempts')
#     return 10
#
#
# # res = get_int_from_user()
# #
# # print(res)
#
#
# def get_numbers_from_string(raw_string):
#     tmp_list = []
#
#     for char in raw_string:
#         if char.isdigit():
#             tmp_list.append(char)
#
#     return ''.join(tmp_list)
#
#
# print(get_numbers_from_string('1a2b3c4d'))

import random


def get_ai_number(min_limit, max_limit):
    ai_number = random.randint(min_limit, max_limit)
    print(f'---> {ai_number}')
    return ai_number


def get_user_number(min_limit, max_limit):
    while True:
        try:
            user_input = int(input('Enter the number: '))
        except ValueError:
            print('Wrong! Enter the int NUMBER!')
        else:
            if min_limit <= user_input <= max_limit:
                return user_input
            print('Wrong number!')


def compare_numbers(ai_value, user_value):
    if ai_value == user_value:
        print('Good!')
        return True
    else:
        print('Try again')
        return False


def game():
    min_value = 1
    max_value = 10
    ai_number = get_ai_number(min_limit=min_value, max_limit=max_value)
    while True:
        user_number = get_user_number(min_limit=min_value, max_limit=max_value)
        is_game_over = compare_numbers(ai_value=ai_number, user_value=user_number)

        if is_game_over:
            print('Game over!')
            return


game()
