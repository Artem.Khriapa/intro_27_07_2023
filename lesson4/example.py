# my_list = ['s', 'd']
# my_list.append(1)
# my_list.append(2)
#
# print(my_list)
#
# my_tuple = ('s', 'd')
#
# my_tuple = my_tuple + (1, 2, 3)
#
#
# # ternary operator
#
# # m = a if a > 0
# # m = -a if a < 0
#
# var = 12342
#
# if var > 0:
#     abs_var = var
# else:
#     abs_var = -var
#
# abs_var = var if var > 0 else -var
#
#
# # left if True else right
#

# mutable data structures behaviour

# my_list = [1, 2, 3, 4]
#
# print(id(my_list))
#
# my_list.append(5)
# print(id(my_list))
#
# a = 10
# print(id(a))
# a += 1
# print(id(a))

# my_list1 = [1, 2, 3, 4]
# my_list2 = my_list1
#
# print('my_list1', my_list1)
# print('my_list2', my_list2)
#
# my_list1.append(5)
#
# print('my_list1', my_list1)
# print('my_list2', my_list2)
#
# print('my_list1', id(my_list1))
# print('my_list2', id(my_list2))


# my_list1 = [1, 2, 3, 4]
# # my_list2 = my_list1[:]
# my_list2 = my_list1.copy()
#
# print('my_list1', my_list1)
# print('my_list2', my_list2)
#
# my_list1.append(5)
#
# print('my_list1', my_list1)
# print('my_list2', my_list2)
#
# print('my_list1', id(my_list1))
# print('my_list2', id(my_list2))


# my_list1 = [1, 2, ['a', 'b']]
#
# my_list2 = my_list1.copy()
#
# print('my_list1', my_list1)
# print('my_list2', my_list2)
#
# my_list1[2].append('c')
# my_list1.append(5)
#
# print('my_list1', my_list1)
# print('my_list2', my_list2)
#
# print('my_list1', id(my_list1))
# print('my_list2', id(my_list2))

# import copy
#
# my_list1 = [1, 2, ['a', 'b']]
#
# my_list2 = copy.deepcopy(my_list1)
# # my_list2 = my_list1[:]
#
# print('my_list1', my_list1)
# print('my_list2', my_list2)
#
# my_list1[2].append('c')
# my_list1.append(5)
#
# print('my_list1 -->', id(my_list1[2]))
# print('my_list2 -->', id(my_list2[2]))
#
# print('my_list1', my_list1)
# print('my_list2', my_list2)
#
# print('my_list1', id(my_list1))
# print('my_list2', id(my_list2))


# data structures
    # set
    # frozenset
    # dict


# set

# my_set = {1, 2.3, False, '1234', None, (1, 2, 3)}
#
# print(my_set)
#
# my_set.add(1.5)
# my_set.add('1.5')
#
# for i in my_set:
#     print(i)
#
# my_set.remove('1.5')
#
# print(my_set)
#
# my_set = set()
# my_set = set([1, 2, 3, 4])
# print(my_set)
# print(type(my_set))
#
# print(1 in my_set)
#
# my_frozenset = frozenset([1, 2, 3, 4])
# print(my_frozenset)
# print(type(my_frozenset))

# my_key = 'login'
#
#
# my_dict = {
#     1: 100,
#     2.2: 100,
#     False: True,
#     None: 3456,
#     (1, 2, 3): [1, 2, 3],
#     my_key: 'qwert',
#     'password': '2345',
# }
#
# print(my_dict)
# print(type(my_dict))
#
#
# print(my_dict[2.2])
# my_dict['False'] = '123456789'
# print(my_dict)
# my_dict['False'] = 'qwertyu'
# print(my_dict)


# my_dict = {
#     1: 100,
#     2.2: 100,
#     False: True,
#     None: 3456,
#     (1, 2, 3): [1, 2, 3],
#     'my_key': 'qwert',
#     'password': '2345',
# }

# for dict_data in my_dict:
#     print('key', dict_data)
#     print('value', my_dict[dict_data])


# for dict_key in my_dict.keys():
#     print('key', dict_key)
#     print('value', my_dict[dict_key])
#
# for dict_value in my_dict.values():
#     print('value', dict_value)

# for dict_item in my_dict.items():
#     print('item', dict_item)
#     print('key', dict_item[0])
#     print('value', dict_item[1])
#
#
# for dict_key, dict_value in my_dict.items():
#     print('key', dict_key)
#     print('value', dict_value)


# my_dict = {
#     1: 123,
# }
# print(my_dict)
#
# my_dict['asdfg'] = 1234567
# print(my_dict)
#
# my_dict1 = {
#     1: 1,
#     2: 2,
#     # 3: 'Hello!'
# }
#
# my_dict.update(my_dict1)
# print(my_dict)
#
# my_dict.pop(1)
#
# print(my_dict)
#
#
# print(3 in my_dict)
#
# res = my_dict[3] if 3 in my_dict else 'some default'
# print(res)
#
# if 3 in my_dict:
#     res = my_dict[3]
# else:
#     res = 'some default'
# print(res)
#
# res = my_dict.get(3, 'some default value')
# print(res)
#
#
# res = my_dict.setdefault('my_key', 'my_value')
#
# print(res)
#
# if 'my_key' in my_dict:
#     res = my_dict['my_key']
# else:
#     my_dict['my_key'] = 'my_value'
#     res = my_dict['my_key']

#
# my_dict = dict(
#     key1=100,
#     key2=200,
# )
# print(my_dict)
#
#
# my_list = [['key1', 2], ['key2', [1, 2, 3]], [1, 2], [2, 3]]
#
# my_dict = dict(my_list)
# print(my_dict)


# my_dict = {}
#
# for number in range(10):
#     my_dict[number] = number ** 2
# print(my_dict)
#
#
# my_list = []
# for number in range(10):
#     my_list.append(number ** 2)
#
# print(my_list)
#
#
# # comprehensions
#
# my_list = [number ** 2 if number % 2 == 0 else number ** 3 for number in range(10)]
#
#
# print(my_list)
#
# my_list = []
# for number in range(10):
#     if number % 2 == 0:
#         my_list.append(number ** 2)
#     else:
#         my_list.append(number ** 3)
#
# print(my_list)


# my_list = [number ** 2 for number in range(10) if number % 2 == 0]
# print(my_list)
#
#
# my_list = []
# for number in range(10):
#     if number % 2 == 0:
#         my_list.append(number ** 2)
#
# print(my_list)


# my_set = {number ** 2 for number in range(10) if number % 2 == 0}
# print(type(my_set))
#
#
# my_dict = {number: number ** 2 for number in range(10)}
# print(type(my_dict))
# print(my_dict)

# my_list = [number ** 2 for number in range(1000**1000) if number % 2 == 0]
# print(len(my_list))

my_generator = (number ** 2 for number in range(1000**1000))
print(my_generator)

my_list = [v**2 for v in range(100)]

for i in [1, 2, 3]:
    print(i)

