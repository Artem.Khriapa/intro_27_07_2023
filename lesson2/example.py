# # str
#
# my_str = '123 123 123'
# res = my_str.replace('23', 'abc')
# print(res)
#
#
# my_str = '---123 123 123---'
# res = my_str.strip('-')
# print(res)
#
#
# my_str = '---123 123 123---'
# res = my_str.lstrip('-')
# print(res)
#
# my_str = '---123 123 123---'
# res = my_str.rstrip('-')
# print(res)
#
#
# my_str = 'aDcd 1234'
# res = my_str.upper()
# print(res)
#
# my_str = 'aDcd 1234'
# res = my_str.lower()
# print(res)
#
#
# my_str = 'aDcd 1234 dsgfaDcd'
# res = my_str.capitalize()
# print(res)
#
#
# my_str = 'aDcd 1234 dsgfaDcd'
# res = my_str.title()
# print(res)
#
#
# my_str = 'abDcd 123ab4 dsgfaDcd ab'
# res = my_str.count('ab')
# print(res)
#
#
# my_str = 'abDcd 123ab4 dsgfaDcd ab'
# res = my_str.startswith('sab')
# print(res)
#
#
# my_str = 'abDcd 123ab4 dsgfaDcd ab'
# res = my_str.endswith(' ab')
# print(res)
#
#
# my_str = '12345678&2345676543212345'
# res = my_str.isdigit()
# print(res)
#
#
# my_str = 'asdsdfgfhnm%'
# res = my_str.isalpha()
# print(res)
#
#
# my_str = 'asdsdfgfhnm 1234'
# res = my_str.isalnum()
# print(res)
#
#
# my_str = 'asdsdfgfhnm 1234'
# res = my_str.index('ds')
# print(res)

# slices
#
# my_str = '0123456789'
# # my_str = 'abcdefghij'
#
# print("my_str[0] ->", my_str[0])
# print("my_str[3] ->", my_str[3])
# # print("my_str[100] ->", my_str[100])
# print("my_str[9] ->", my_str[9])
#
# print("my_str[-1] ->", my_str[-1])
# print("my_str[-2] ->", my_str[-2])
#
# print("my_str[2:5] ->", my_str[2:5])
# print("my_str[:5] ->", my_str[:5])
# print("my_str[2:] ->", my_str[2:])
# print("my_str[-5:] ->", my_str[-5:])
# print("my_str[-5:-2] ->", my_str[-5:-2])
# print("my_str[6:2] ->", my_str[6:2])
#
# print("my_str[:] ->", my_str[:])
# print("my_str[::2] ->", my_str[::2])
# print("my_str[::-1] ->", my_str[::-1])
#
# print(len(my_str))
# print("my_str[::2] ->", my_str[len(my_str) // 2::-1])


# var1 = 110
# var2 = 20
#
#
# if var1 > var2:  # false 10 > 20
#     print(f'{var1} > {var2}')
# else:
#     print(f'{var1} <= {var2}')


# var1 = 110
# var2 = 20
#
#
# if var1 >= var2:  # false 10 > 20
#     print(f'{var1} > {var2}')
#     if var1 == var2:
#         print(f'{var1} = {var2}')
#     else:
#         print(f'{var1} != {var2}')
# else:
#     print(f'{var1} <= {var2}')
#
# print('The end')


# var1 = 0
# var2 = 20
#
#
# if var1:  # bool(var1)
#     print(f'Var1 is: {var1}')
# else:
#     print(f'{var1} <= {var2}')
#
# print('The end')


# var1 = 10
#
#
# if var1:  # bool(var1)
#     print(f'Var1 is: {var1}')
#
#
# print('The end')

# var1 = 20
# var2 = 20
#
# if var1 > var2:
#     print(f'{var1} > {var2}')
# elif var1 < var2:
#     print(f'{var1} < {var2}')
# else:
#     print(f'{var1} = {var2}')
#
# print('The end')


# var1 = 20
# var2 = 20
#
# if var1 > var2:
#     print(f'{var1} > {var2}')
# elif var1 < var2:
#     print(f'{var1} < {var2}')
# elif var1 > 10:
#     print(f'{var1} > 10')
#
#
# print('The end')


# user_input = input('Enter something: ')
# print(type(user_input))
#
# print(f'You enter: {user_input}')
#
# if user_input.isdigit():
#     var = int(user_input)
#     print(type(var))

# var1 = 10
# var2 = 20
#
# while var2 > var1:
#     print(f'{var2} {var1}')
#
#     var1 += 1  # var1 = var1 + 1
#
# print('The end')


# var1 = 5
#
# while var1:  # bool(var1)
#     print(f'{var1}')
#
#     var1 -= 1  # var1 = var1 - 1
#
# print('The end')


# var1 = 5
#
# while True:
#
#     var1 -= 1  # var1 = var1 - 1
#
#     if var1 % 2 == 0:
#         print(f'{var1}')
#         continue
#
#     if var1 <= 0:
#         break
#
# print('The end')


# while True:
#     user_input = input('Enter the number: ')
#     if user_input.isdigit():
#         user_input = int(user_input)
#         print('Thank you!')
#         break
#     else:
#         print('Wrong! Enter the NUMBER!')


var1 = 10
var2 = 20

if var1 > var2 or var1 < var2:
    print(f'{var1} > {var2} or {var1} < {var2}')
else:
    print(f'{var1} == {var2}')


res = var1 + var2 ** 2 * 3

"""
()
**
* / // %
+ -
not
and
or
=
-

"""

if not var1 > var2:
    print(f'{var1} > {var2} or {var1} < {var2}')
else:
    print(f'{var1} == {var2}')


res = 0 or 'wer' or 100
print(res)

var1 = 30
var2 = 20


if var1 > var2 and var1 > 10:
    print(f'{var1} > {var2} or {var1} > 10')
else:
    print(f'{var1} == {var2}')


res = 0 and 100
print(res)


if var1 > var2 and (var1 > 10 or var1 < 0):
    print(f'{var1} > {var2} or {var1} > 10')
else:
    print(f'{var1} == {var2}')

