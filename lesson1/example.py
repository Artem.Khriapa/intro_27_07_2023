# a = 10
# b = 3.0
# c = a + b
# print(c)

# b = 'jsshfbglfj'  # dlkfjbmf'pg[
#
# abcd1 = 110
# a1bcd = 110

# Abcd = 110
# abcd = 210

# students_counter = 110
# abcd = 210


# Base data types

# numbers

# int
#
# my_int1 = 1245
# my_int2 = 2245
#
# print(type(my_int1))
#
# result = my_int2 + my_int1
# print(result)
#
# result = my_int2 - my_int1
# print(result)
#
# result = my_int2 * my_int1
# print(result)
#
# result = my_int2 / my_int1  # ! 0
# print(result)
#
# result = my_int2 // my_int1  # ! 0
# print(result)
#
# result = my_int2 % my_int1
# print(result)
#
# result = my_int2 ** my_int1
# print(result)
#
# result = my_int2 / my_int1  # ! 0
# print(type(result))
#
#
# # float
#
# my_float1 = 1.25
# my_float2 = 3.25
#
#
# result = my_float1 + my_float2
# print(result)
#
#
# result = my_int1 + my_float2
# print(result)
#
# print(0.1 + 0.2)
#
# my_float1 = 0.0000000000000001
#
# print(my_float1)
#
# my_float1 = 1e-16  # 1 * (10 ** -16)
#
# result = 10 + my_float1
#
# print(result)
#
# # bool
#
# my_bool1 = True
# my_bool2 = False
#
# res = my_int2 > my_float1
# print(res)
#
# res = my_int2 < my_float1
# print(res)
#
# res = my_int2 <= my_float1
# print(res)
#
# res = my_int2 >= my_float1
# print(res)
#
# res = my_int2 == my_float1
# print(res)
#
# res = my_int2 != my_float1
# print(res)


#
#
# my_float = 1.93
#
# my_int = int(my_float)
# print(my_int)
#
# my_float = float(my_int)
# print(my_float)
#
# my_bool = bool(my_int)
# print(my_bool)
#
# my_bool = bool(0.0)
# print(my_bool)
#
#
# # nonetype
#
# my_none = None

# str
#
# my_str = 'hgfscdfcuytuywcv'
#
# # my_str = "hgfscdfcuytuywcv"
# print(my_str)
# print(type(my_str))
#
# my_str = "abcd\"ef"
# print(my_str)
# print(type(my_str))
#
# my_str = 'abcd\'efa sw\ndusygefiygrehurjiohjiewurhgiysegfiuywegfouygweiafuycgivyoipuy5gbfw\erg'
# print(my_str)
#
# my_str = '''234567
# 234567
# qwertyu
# swdefrgthyju
# asdfgh
# '''
# print(my_str)
#
# my_str = '12345'
#
# my_int = int(my_str)
# print(my_int)
# print(type(my_int))
#
#
# my_str = '123.45'
#
# my_float = float(my_str)
# print(my_float)
# print(type(my_float))
#
# my_bool = bool('')
# print(my_bool)
#
#
# my_str1 = '123.45'
# my_str2 = 'ydgoivoubpfig['
#
# res = my_str1 + my_str2 + '3456'
# print(res)
#
#
# res = 'abc' * 3
# print(res)

# value = 123
#
# value = str(True)
#
# res = 'Hello' + value + ' ' + 'World'
# print(res)
#
#
# res = 'Hello' + str(True) + ' ' + 'World'
# print(res)

# string formatting

name = 'Artem'
age = 40


# Hello, my name is Artem, I'm 40

res = 'Hello, my name is ' + name + ', I\'m ' + str(age)
print(res)

tpl = 'Hello, my name is %s, I\'m %s'
res = tpl % (name, age)
print(res)


tpl = 'Hello, my name is {}, I\'m {}'
res = tpl.format(name, age)
print(res)

res = 'Hello, my name is {}, I\'m {}'.format(name, age)
print(res)

res = 'Hello, my name is {0}, I\'m {1}'.format(name, age)
print(res)

res = 'Hello, my name is {name_value}, I\'m {age_value}'.format(age_value=age, name_value=name)
print(res)

res = f'Hello, my name is {name}, I\'m {age}'
print(res)


# task 1
#



# task 2
#

