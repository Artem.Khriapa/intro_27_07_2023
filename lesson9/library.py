from random import randint

variable1 = 120

def foo(var):
    print(f'---> FOO ', var)


def get_ai_number(min_limit: int, max_limit: int) -> int:
    ai_number = randint(min_limit, max_limit)
    print(f'---> {ai_number}')
    return ai_number


def get_user_number(min_limit, max_limit):
    while True:
        try:
            user_input = int(input('Enter the number: '))
        except ValueError:
            print('Wrong! Enter the int NUMBER!')
        else:
            if min_limit <= user_input <= max_limit:
                return user_input
            print('Wrong number!')


def compare_numbers(ai_value, user_value):
    if ai_value == user_value:
        print('Good!')
        return True
    else:
        print('Try again')
        return False


if __name__ == '__main__':
    print(f'__name__ is {__name__}')
    foo('library')
