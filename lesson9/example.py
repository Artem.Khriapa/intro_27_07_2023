# import random
#
# # imports
#
# print(random.random())

# # -------------------------
#
# from random import randint
#
# print(randint(1, 10))


# # -------------------------
#
# from random import randint, choice
#
# print(randint(1, 10))
# print(choice([1, 11, 2, 3, 4, 5, 10]))

# # -------------------------
#
# from random import randint, choice
#
# print(randint(1, 10))
# print(choice([1, 11, 2, 3, 4, 5, 10]))

# # -------------------------
#
# import random
# import requests
# print(random.randint(1, 10))

# # -------------------------
#
# import random as R
#
# print(R.random())

# # -------------------------
#
# from random import randint as RI
#
# print(RI(1, 10))

# # -------------------------
#
# from random import randint as RI
# from random import random as R
#
# print(RI(1, 10))
# print(R())


# # -------------------------
#
# print(dir())
#
# from random import *
#
# print(dir())
# print(randint(1, 10))

# # -------------------------
# from library import get_ai_number, get_user_number, compare_numbers
#
#
# def game():
#     min_value = 1
#     max_value = 10
#     ai_number = get_ai_number(min_limit=min_value, max_limit=max_value)
#     while True:
#         user_number = get_user_number(min_limit=min_value, max_limit=max_value)
#         is_game_over = compare_numbers(ai_value=ai_number, user_value=user_number)
#
#         if is_game_over:
#             print('Game over!')
#             return
#
#
# game()


# -------------------------
# import library
#
# print(dir(library))
#
# print(library.variable1)

# # -------------------------
# # import library  # python3 library.py
# from library import foo  # python3 library.py
#
# # library.foo('main file')
# foo('main file')
#
# print(f'__name__ is {__name__}')

# # -------------------------

# import lib2

# from lib2 import a
# print(a.vara)

# import lib2
#
# print(lib2.vara)
# print(lib2.varb)


# annotation

# # -------------------------
# from library import get_ai_number, get_user_number, compare_numbers
#
#
# def game():
#     min_value = 1
#     max_value = 10
#     ai_number = get_ai_number(min_limit=min_value, max_limit=max_value)
#     while True:
#         user_number = get_user_number(min_limit=min_value, max_limit=max_value)
#         is_game_over = compare_numbers(ai_value=ai_number, user_value=user_number)
#
#         if is_game_over:
#             print('Game over!')
#             return
#
#
# # game()
#
#
# def foo(a: int, b: str) -> bool:
#     print(a, b)
#     return bool(a)
#
#
# res = foo('sdfg', [1, 2, 3])
# foo('sdfg', res)
#
# print(foo.__annotations__)
#
#
# def foo(a, b):
#     """
#     Args:
#         a (int):
#         b (str):
#
#     Returns:
#         (bool):
#     """
#     print(a, b)
#     return bool(a)
#
#
# res = foo('sdfg', [1, 2, 3])
# foo('sdfg', res)
# print(foo.__doc__)
#

# useful functions


# my_int = int('12345')


# res = min(1, 2, 3, 4, 5, -2)
# res = max(1, 2, 3, 4, 5, -2)

# def foo(v):
#     return v % 3
#
# res = max([1, 2, 3, 4, 5, -2], key=foo)
#
# res = max([1, 2, 3, 4, 5, -2], key=lambda val: val % 3)

#
# lst = [1, 2, 3, 4, 5, -2]
#
# res_lst = []
# for item in lst:
#     if item % 2 == 0:
#         res_lst.append(item)
#
# print(res_lst)
#
#
# def foo(val):
#     return val % 2 == 0
#
# res_lst = list(filter(foo, lst))
#
# res_lst = list(filter(lambda val: val % 2 == 0, lst))
#
# print(res_lst)

#
# lst = [1, 2, 3, 4, 5, -2]
#
# for item in lst:
#     val = item % 2
#     print(val)
#
# mapped = map(lambda v: v % 2, lst)
#
# for item in mapped:
#     print(item)

# for item in map(lambda v: v % 2, lst):
#     print(item)

# lst = [1, 2, 3, 4, 5, -2]
# string = 'abcdef'
# st = {1, 2, 3}

# for i in range(6):
#     print(lst[i], string[i])

# zipped = zip(lst, string, range(5))
# for item in zipped:
#     print(item)


# lambda

# print(lambda x: x**2)
# print(lambda x, y: x**2 + y**2)

#
# my_function = lambda x: x**2  # bad idea!
#
# print(my_function(3))


# print(lambda x: int(x) + 1)  # bad idea!


# lambda *args, **kwargs: smth with args, kwargs


dct = {
    '+': lambda x, y: x + y,
    '-': lambda x, y: x - y,
    '*': lambda x, y: x * y,
    '/': lambda x, y: x / y,
}

print(dct['+'](2, 3))


