# Descriptor
#   property
#   calculable attributes
# Magic methods
# Association
#   aggregation
#   composition


# class User:
#     # class attributes
#     name = 'Artem'
#     age = 40
#
#     def __init__(self, user_name, user_age):
#         # print('in init', self.name, self.age)
#         self.name = user_name
#         self.age = user_age
#
#     def say_hello(self, prefix='Hi'):
#         res = f'{prefix}, my name is {self.name} I\'m {self.age} years old'
#         return res
#
#
# class Programmer(User):
#     language = 'Python'
#
#     def __init__(self, name, age, language):
#         super().__init__(name, age)
#         self.language = language
#
#     def make_program(self):
#         return f'Program on {self.language}'
#
#     def say_hello(self, prefix='Hi'):
#         res = f'{prefix}, my name is {self.name} I\'m {self.age} years old, {self.language}'
#         return res
#
#
# p1 = Programmer('Bill', 50, 'PHP')
#
# # print(p1.name)
# # print(p1.age)
# # print(p1.language)
#
# print(User.name)
# print(Programmer.language)

#
# class Point:
#     x = 0
#     y = 0
#
#     def __init__(self, x, y):
#         if isinstance(x, (int, float)) and isinstance(y, (int, float)):
#             self.x = x
#             self.y = y
#         else:
#             raise TypeError
#
#     def __str__(self):
#         return f'Point [{self.x}:{self.y}]'
#
#
# p1 = Point(1, 1)
# p2 = Point(5, 5)
#
# print(p1)
# print(p2)
#
#
# class Line:
#     begin = None
#     end = None
#
#     def __new__(cls, *args, **kwargs):
#         instance = object.__new__(cls)
#         return instance
#
#     def __init__(self, point1, point2):  # aggregation
#         self.begin = point1
#         self.end = point2
#
#     # def __init__(self, x1, y1, x2, y2):  # composition
#     #     self.begin = Point(x1, y1)
#     #     self.end = Point(x2, y2)
#
#     def length(self):
#         diffx = self.begin.x - self.end.x
#         diffy = self.begin.y - self.end.y
#
#         res = (diffx ** 2 + diffy ** 2) ** 0.5
#
#         return res
#
#
#
#
# p1 = Point(0, 0)
# p2 = Point(3, 4)
#
# line1 = Line(p1, p2)  # aggregation
#
# p3 = Point(2, 3)
# p4 = Point(5, 8)
#
# line2 = Line(p3, p4)  # aggregation
# # line = Line(1, 1, 5, 5)  # composition
#
# # print(line1)
# # print(line1.begin)
# # print(line1.end)
# # print(line1.length())
#
#
# print(line1.length() < line2.length())
#

#
# class Point:
#     x = 0
#     y = 0
#
#     def __init__(self, x, y):
#         if isinstance(x, (int, float)) and isinstance(y, (int, float)):
#             self.x = x
#             self.y = y
#         else:
#             raise TypeError
#
#     def __str__(self):
#         return f'Point [{self.x}:{self.y}]'
#
#     def __add__(self, other):
#         if not isinstance(other, self.__class__):
#             raise TypeError
#
#         return Line(self, other)
#
#
# class Line:
#     begin = None
#     end = None
#
#     def __init__(self, point1, point2):
#         if isinstance(point1, Point) and isinstance(point2, Point):
#             self.begin = point1
#             self.end = point2
#         else:
#             raise TypeError
#
#     def length(self):
#         diffx = self.begin.x - self.end.x
#         diffy = self.begin.y - self.end.y
#
#         res = (diffx ** 2 + diffy ** 2) ** 0.5
#
#         return res
#
#     def __str__(self):
#         return f'Line [{self.begin} - {self.end}]'
#
#     def __eq__(self, other):
#         print('in __eq__')
#         return self.length() == other.length()
#
#     def __ne__(self, other):
#         print('in __ne__')
#         return self.length() != other.length()
#
#     def __gt__(self, other):
#         print('in __gt__')
#         return self.length() > other.length()
#
#     def __ge__(self, other):
#         print('in __ge__')
#         return self.length() >= other.length()
#
#     def __lt__(self, other):
#         print('in __lt__')
#         return self.length() < other.length()
#
#     def __le__(self, other):
#         print('in __le__')
#         return self.length() <= other.length()
#
#
# p1 = Point(0, 0)
# p2 = Point(3, 4)
#
# line1 = Line(p1, p2)
#
# p3 = Point(2, 3)
# p4 = Point(5, 8)
#
# line2 = Line(p3, p4)
#
# print(line1 != line2)
#
#
# line3 = p1 + p3
#
# p1.x = '1234asdf'
#
# print(line3.length())

#
# class Point:
#     x = 0
#     y = 0
#
#     def __init__(self, x, y):
#         if isinstance(x, (int, float)) and isinstance(y, (int, float)):
#             self.x = x
#             self.y = y
#         else:
#             raise TypeError
#
#     def __str__(self):
#         return f'Point [{self.x}:{self.y}]'
#
#     def __add__(self, other):
#         if not isinstance(other, self.__class__):
#             raise TypeError
#
#         return Line(self, other)
#
#
# class Line:
#
#     _begin = None  # protected
#     _end = None
#
#     def begin_getter(self):
#         print('begin_getter')
#         return self._begin
#
#     def begin_setter(self, value):
#         print(f'begin_setter {value}')
#         if isinstance(value, Point):
#             self._begin = value
#         else:
#             raise TypeError
#
#     begin = property(begin_getter, begin_setter)
#
#     def end_getter(self):
#         print('end_getter')
#         return self._end
#
#     def end_setter(self, value):
#         print(f'end_setter {value}')
#         if isinstance(value, Point):
#             self._end = value
#         else:
#             raise TypeError
#
#     end = property(end_getter, end_setter)
#
#     def __init__(self, point1, point2):
#         self.begin = point1
#         self.end = point2
#
#     def length_getter(self):
#         # print('in get_length')
#         diffx = self.begin.x - self.end.x
#         diffy = self.begin.y - self.end.y
#
#         res = (diffx ** 2 + diffy ** 2) ** 0.5
#
#         return res
#
#     def length_setter(self, value):
#         print(f'in length_setter {value}')
#         raise NotImplementedError
#
#     length = property(length_getter)
#     # length = property(length_getter, length_setter)
#
#     def __str__(self):
#         return f'Line [{self.begin} - {self.end}]'
#
#     def __eq__(self, other):
#         print('in __eq__')
#         return self.length_getter() == other.length_getter()
#
#     def __ne__(self, other):
#         print('in __ne__')
#         return self.length_getter() != other.length_getter()
#
#     def __gt__(self, other):
#         print('in __gt__')
#         return self.length_getter() > other.length_getter()
#
#     def __ge__(self, other):
#         print('in __ge__')
#         return self.length_getter() >= other.length_getter()
#
#     def __lt__(self, other):
#         print('in __lt__')
#         return self.length_getter() < other.length_getter()
#
#     def __le__(self, other):
#         print('in __le__')
#         return self.length_getter() <= other.length_getter()
#
#
# p1 = Point(0, 0)
# p2 = Point(3, 4)
#
# line1 = Line(p1, p2)
#
# p3 = Point(2, 3)
# p4 = Point(5, 8)
#
# line2 = p3 + p4
#
# print(line2.length)
#
# # line2.begin = 'p1'
#
# print(line2.length)

class Line:

    _begin = None  # protected
    _end = None

    def begin_getter(self):
        print('begin_getter')
        return self._begin

    def begin_setter(self, value):
        print(f'begin_setter {value}')
        if isinstance(value, Point):
            self._begin = value
        else:
            raise TypeError

    begin = property(begin_getter, begin_setter)

    def end_getter(self):
        print('end_getter')
        return self._end

    def end_setter(self, value):
        print(f'end_setter {value}')
        if isinstance(value, Point):
            self._end = value
        else:
            raise TypeError

    end = property(end_getter, end_setter)

    def __init__(self, point1, point2):
        self.begin = point1
        self.end = point2

    @property
    def length(self):
        # print('in get_length')
        diffx = self.begin.x - self.end.x
        diffy = self.begin.y - self.end.y

        res = (diffx ** 2 + diffy ** 2) ** 0.5

        return res

    @length.setter
    def length(self, value):
        print(f'in length_setter {value}')
        raise NotImplementedError


class Point:
    x = 0
    y = 0

    def __init__(self, x, y):
        if isinstance(x, (int, float)) and isinstance(y, (int, float)):
            self.x = x
            self.y = y
        else:
            raise TypeError

    def __str__(self):
        return f'Point [{self.x}:{self.y}]'

    def foo(self):
        return self.__class__.x

    @classmethod
    def bar(cls):
        return cls.x

    @staticmethod
    def baz():
        return 'hello'


p3 = Point(2, 3)

print(p3.foo())
print(p3.bar())
print(Point.bar())
print(Point.x)
