from flask import Flask, render_template, request

app = Flask(__name__)


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/hello')
def hello():
    return 'Hello World'


@app.route('/hellouser')
def hellouser():
    username = 'Bill'

    return render_template('hellouser.html', username=username)


@app.route('/path/<order_id>')
def path_function(order_id):

    return render_template('order_id.html', order_id=order_id)


@app.route('/query')
def query_function():
    query_args = dict(request.args)

    username = 'Artem'
    return render_template('query.html', username=username, data=query_args)
