# Object Oriented Programming


# main concepts
#   Abstraction
#   Encapsulation
#   Inheritance
#   Polymorphism

# class
# object


# my_int = 100

#
# class User:
#
#     name = 'Artem'
#     age = 40
#
#
# user1 = User()
#
# print(user1)
# print(type(user1))
#
# print(user1.name)
# print(user1.age)
#
# user2 = User()
# user3 = User()
#
# print(user3.name)
# print(user2.name)
#
# user1.name = 'Bill'
# user3.name = 'Bob'
#
# print(user3.name)
# print(user1.name)


# class User:
#     name = 'Artem'
#     age = 40
#
#     def say_hello(self):
#         res = f'Helo, my name is {self.name} I\'m {self.age} years old'
#         return res
#
#     def say_hello_prefix(self, prefix='Hi'):
#         res = f'{prefix}, my name is {self.name} I\'m {self.name} years old'
#         return res
#
#
# user1 = User()
# print(user1.say_hello())  # print(User.say_hello(user1))
#
# user1.name = 'Tom'
# user1.age = 20
# print(user1.say_hello())
#
# user2 = User()
# print(user2.say_hello_prefix())


# class User:
#     name = 'Artem'
#     age = 40
#
#     def __init__(self, user_name, user_age):
#         # print('in init', self.name, self.age)
#         self.name = user_name
#         self.age = user_age
#
#     def say_hello(self, prefix='Hi'):
#         res = f'{prefix}, my name is {self.name} I\'m {self.age} years old'
#         return res
#
#
# user1 = User(user_name='Bill', user_age=60)
# print(user1.say_hello())
# user2 = User(user_name='Elon', user_age=50)
# print(user2.say_hello('Hello Mars'))


# class User:
#     # name = 'Artem'
#     # age = 40
#
#     def __init__(self, user_name, user_age):
#         # print('in init', self.name, self.age)
#         self.name = user_name
#         self.age = user_age
#
#     def say_hello(self, prefix='Hi'):
#         res = f'{prefix}, my name is {self.name} I\'m {self.age} years old'
#         return res
#
#
# class Programmer(User):
#     language = 'Python'
#
#     def make_program(self):
#         return f'Program on {self.language}'
#
#     def say_hello(self, prefix='Hi'):
#         res = f'{prefix}, my name is {self.name} I\'m {self.age} years old, {self.language}'
#         return res
#
#
# user1 = User(user_name='Bill', user_age=60)
# print(user1.say_hello())
#
# programmer1 = Programmer(user_name='Bill', user_age=60)
# print(programmer1.language)
# print(programmer1.say_hello())
# print(programmer1.make_program())
#
#
# print(dir(programmer1))


# class User:
#     name = 'Artem'
#     age = 40
#
#     def __init__(self, user_name, user_age):
#         # print('in init', self.name, self.age)
#         self.name = user_name
#         self.age = user_age
#
#     def say_hello(self, prefix='Hi'):
#         res = f'{prefix}, my name is {self.name} I\'m {self.age} years old'
#         return res
#
#
# class Programmer(User):
#     language = 'Python'
#
#     # def __init__(self, *args, **kwargs):
#     #     self.language = 'Python'
#
#     def make_program(self):
#         return f'Program on {self.language}'
#
#     def say_hello(self, prefix='Hi'):
#         res = f'{prefix}, my name is {self.name} I\'m {self.age} years old, {self.language}'
#         return res
#
#
# user1 = User(user_name='Bill', user_age=60)
# # print(user1.say_hello())
#
# programmer1 = Programmer(user_name='Bill', user_age=60)
# print(programmer1.language)
# print(programmer1.say_hello())
# print(programmer1.make_program())
#
#
# print(dir(programmer1))
#
# print(isinstance(user1, User))
# print(isinstance(programmer1, Programmer))
# print(isinstance(programmer1, User))

# print(hasattr(programmer1, 'language'))
#
# attr_name = 'language'
#
# print(hasattr(programmer1, attr_name))
# print(hasattr(programmer1, 'hggsfdc'))


# print(getattr(programmer1, 'say_hello', None))
#
# func = getattr(programmer1, 'say_hello', None)
# print(func())
#
# func = programmer1.say_hello
# print(func())


# programmer1.my_random_attr = 100
# print(programmer1.my_random_attr)

# setattr(programmer1, 'my_random_attr', 100)
# print(programmer1.my_random_attr)
#
# print(programmer1.say_hello())
# delattr(programmer1, 'my_random_attr')
# # print(programmer1.my_random_attr)
#
# programmer2 = Programmer('Abc', 10)
# print(programmer2.name)


class A:
    attr = 'a'
    pass


class B:
    # attr = 'b'
    pass


class C(A, B):
    # attr = 'c'
    pass


class E():
    # attr = 'e'
    pass


class D(E, C):
    attr = 'd'
    pass


obj_d = D()

print(obj_d.attr)

print(D.mro())
